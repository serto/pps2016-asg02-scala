package game;

import model.Model;

import javax.swing.*;

/**
 * Created by Matteo on 15/03/2017.
 */
public class GamePanel {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";

    public GamePanel(){
        JFrame window = new JFrame(WINDOW_TITLE);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.pack();
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setVisible(true);


        Thread timer = new Thread(new Model(window));
        timer.start();
    }
}
