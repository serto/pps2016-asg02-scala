package controller

import game.Audio
import game.Main
import model.Model
import utils.Res
import view.View
import java.awt.event.KeyEvent
import java.awt.event.KeyListener

object Keyboard{
  private val MOVING_FORWARD = 1
  private val MOVING_BACK = -1
}

import Keyboard._
class Keyboard(var gameView: View) extends KeyListener {

  override def keyPressed(e: KeyEvent): Unit = if (this.gameView.getGameModel.getMario.isAlive) {
    if (e.getKeyCode == KeyEvent.VK_RIGHT) { // per non fare muovere il castello e start
      if (this.gameView.getGameModel.getxPosScreen == -1) {
        this.gameView.getGameModel.setMovement(0)
        this.gameView.setBackground1PosX(-50)
        this.gameView.setBackground2PosX(750)
      }
      this.gameView.getGameModel.getMario.setMoving(true)
      this.gameView.getGameModel.getMario.setToRight(true)
      this.gameView.getGameModel.setMovement(MOVING_FORWARD) // si muove verso sinistra
    }
    else if (e.getKeyCode == KeyEvent.VK_LEFT) {
      if (this.gameView.getGameModel.getxPosScreen == 4601) {
        this.gameView.getGameModel.setxPosScreen(4600)
        this.gameView.setBackground1PosX(-50)
        this.gameView.setBackground2PosX(750)
      }
      this.gameView.getGameModel.getMario.setMoving(true)
      this.gameView.getGameModel.getMario.setToRight(false)
      this.gameView.getGameModel.setMovement(MOVING_BACK) // si muove verso destra
    }
    // salto
    if (e.getKeyCode == KeyEvent.VK_UP) {
      this.gameView.getGameModel.getMario.setJumping(true)
      Audio.playSound(Res.AUDIO_JUMP)
    }
  }

  override def keyReleased(e: KeyEvent): Unit = {
    this.gameView.getGameModel.getMario.setMoving(false)
    this.gameView.getGameModel.setMovement(0)
  }

  override def keyTyped(e: KeyEvent): Unit = {
  }
}