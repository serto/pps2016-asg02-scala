package model

import gameObjects.Block
import gameObjects.Piece
import gameObjects.Tunnel
import java.util
import java.util.Collections

/**
  * Created by Matteo on 13/03/2017.
  */
class CreationGameObjects(var gameModel: Model) {

  private val obstacleTunnel = new util.ArrayList[Tunnel]
  private val obstacleBlock = new util.ArrayList[Block]
  private val obstaclePiece = new util.ArrayList[Piece]
  this.addObstacleTunnel()
  this.addObstacleBlock()
  this.addObstaclePiece()

  private def addObstacleTunnel() = {
    obstacleTunnel.add(new Tunnel(600, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(1000, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(1600, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(1900, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(2500, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(3000, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(3800, 230, this.gameModel))
    obstacleTunnel.add(new Tunnel(4500, 230, this.gameModel))
  }

  private def addObstacleBlock() = {
    obstacleBlock.add(new Block(400, 180, this.gameModel))
    obstacleBlock.add(new Block(1200, 180, this.gameModel))
    obstacleBlock.add(new Block(1270, 170, this.gameModel))
    obstacleBlock.add(new Block(1340, 160, this.gameModel))
    obstacleBlock.add(new Block(2000, 180, this.gameModel))
    obstacleBlock.add(new Block(2600, 160, this.gameModel))
    obstacleBlock.add(new Block(2650, 180, this.gameModel))
    obstacleBlock.add(new Block(3500, 160, this.gameModel))
    obstacleBlock.add(new Block(3550, 140, this.gameModel))
    obstacleBlock.add(new Block(4000, 170, this.gameModel))
    obstacleBlock.add(new Block(4200, 200, this.gameModel))
    obstacleBlock.add(new Block(4300, 210, this.gameModel))
  }

  private def addObstaclePiece() = {
    obstaclePiece.add(new Piece(402, 145, this.gameModel))
    obstaclePiece.add(new Piece(1202, 140, this.gameModel))
    obstaclePiece.add(new Piece(1272, 95, this.gameModel))
    obstaclePiece.add(new Piece(1342, 40, this.gameModel))
    obstaclePiece.add(new Piece(1650, 145, this.gameModel))
    obstaclePiece.add(new Piece(2650, 145, this.gameModel))
    obstaclePiece.add(new Piece(3000, 135, this.gameModel))
    obstaclePiece.add(new Piece(3400, 125, this.gameModel))
    obstaclePiece.add(new Piece(4200, 145, this.gameModel))
    obstaclePiece.add(new Piece(4600, 40, this.gameModel))
  }

  def getObstacleTunnel: util.List[Tunnel] = Collections.unmodifiableList(obstacleTunnel)

  def getObstacleBlock: util.List[Block] = Collections.unmodifiableList(obstacleBlock)

  def getObstaclePiece: util.List[Piece] = Collections.unmodifiableList(obstaclePiece)
}