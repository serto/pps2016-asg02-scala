package model

import gameObjects._
import controller.Keyboard
import view.View
import javax.swing._
import java.awt._
import java.util
import java.util.Collections

/**
  * Created by Matteo on 13/03/2017.
  */
object Model{
  private val TIME_SLEEP = 3
  private val LENGTH_MAP = 4600
  private val FLAG_X_POS = 4650
  private val FLAG_Y_POS = 115
}

import Model._
class Model(private var gameFrame: JFrame) extends Runnable {
  private val mario : MainCharacter = new MainCharacter(this)
  private val mushroom : Goomba = new Goomba(this)
  private val turtle : Koopa = new Koopa(this)
  private val flag: Flag = new Flag(FLAG_X_POS, FLAG_Y_POS, this);
  private val creationGameObjects : CreationGameObjects = new CreationGameObjects(this)

  private val view : View = new View(this)
  private val obstacles = new util.ArrayList[GameComponent]
  private val pieces = new util.ArrayList[Piece]
  private var xPosScreen = -1
  private var movement = 0


  this.obstacles.addAll(creationGameObjects.getObstacleTunnel)
  this.obstacles.addAll(creationGameObjects.getObstacleBlock)
  this.pieces.addAll(creationGameObjects.getObstaclePiece)

  this.gameFrame.getContentPane.add(this.view)
  this.gameFrame.setVisible(true)
  this.gameFrame.addKeyListener(new Keyboard(this.view))

  override def run: Unit = {
    while (true) {
      this.view.repaint()
      this.checkCollision
      this.moveOjects
      try {
        Thread.sleep(TIME_SLEEP)
      }
      catch {
        case e: InterruptedException => println(e)
      }
    }
  }

  private def checkCollision = {

    for (i <- 0 until obstacles.size - 1) {
      if (this.mario.isNearby(this.obstacles.get(i))) this.mario.contact(this.obstacles.get(i))
      if (this.mushroom.isNearby(this.obstacles.get(i))) this.mushroom.contact(this.obstacles.get(i))
      if (this.turtle.isNearby(this.obstacles.get(i))) this.turtle.contact(this.obstacles.get(i))
    }
    if (this.mushroom.isNearby(turtle)) this.mushroom.contact(turtle)
    if (this.turtle.isNearby(mushroom)) this.turtle.contact(mushroom)
    if (this.mushroom.isNearby(mario) && this.mushroom.isAlive) this.mushroom.contact(mario)
    if (this.turtle.isNearby(mario) && this.turtle.isAlive) this.turtle.contact(mario)
    if (this.mario.isNearby(mushroom) && this.mushroom.isAlive) this.mario.contact(mushroom)
    if (this.mario.isNearby(turtle) && this.turtle.isAlive) this.mario.contact(turtle)
    if (this.mario.isNearby(flag)){ this.mario.contact(flag)
    }

    for(i <- 0 until pieces.size() - 1) {
      if (this.mario.contactPiece(this.pieces.get(i))) {
        this.view.playMoneySound
        this.pieces.remove(i)
      }
    }
  }

  private def moveOjects =
    if (this.xPosScreen >= 0 && this.xPosScreen <= LENGTH_MAP) {
    for (i <- 0 until this.obstacles.size - 1) {
      this.obstacles.get(i).move()
    }
    for(i <- 0 until this.pieces.size - 1) {
      this.pieces.get(i).move()
    }
      this.flag.move();
  }

  def getxPosScreen: Int = this.xPosScreen

  def setMovement(mov: Int): Unit = this.movement = mov

  def getMovement: Int = this.movement

  def setxPosScreen(): Unit = this.xPosScreen = this.xPosScreen + this.movement

  def setxPosScreen(value: Int): Unit = this.xPosScreen = value

  def getObstacles: util.List[GameComponent] = Collections.unmodifiableList(this.obstacles)

  def getPieces: util.List[Piece] = Collections.unmodifiableList(this.pieces)

  def getMario: MainCharacter = this.mario

  def getMushroom: Goomba = this.mushroom

  def getTurtle: Koopa = this.turtle

  def getFlag: Flag = this.flag
}