package gameObjects;

import model.Model;
import utils.Res;

import java.awt.*;

/**
 * Created by Matteo on 18/03/2017.
 */
public class Koopa extends AnimatedEnemy {

    private final static int X_POSITION_TURTLE = 950;
    private final static int Y_POSITION_TURTLE = 243;
    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Koopa(Model model) {
        super(new Point(X_POSITION_TURTLE, Y_POSITION_TURTLE), new Dimension(WIDTH, HEIGHT),
                Res.IMG_TURTLE_IDLE, Res.IMG_TURTLE_DEAD, model);
    }
}
