package gameObjects;

import model.Model;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Matteo on 18/03/2017.
 */
public class Goomba extends AnimatedEnemy {

    private final static int X_POSITION_MUSHROOM = 800;
    private final static int Y_POSITION_MUSHROOM = 263;
    public static final int WIDTH = 27;
    public static final int HEIGHT = 30;

    public Goomba(Model model) {
        super(new Point(X_POSITION_MUSHROOM, Y_POSITION_MUSHROOM), new Dimension(WIDTH, HEIGHT),
                Res.IMG_MUSHROOM_DEFAULT, Res.IMG_MUSHROOM_DEAD_DX, model);
    }
}
